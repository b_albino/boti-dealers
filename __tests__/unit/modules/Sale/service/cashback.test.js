import CashbackService from '../../../../../src/app/modules/Sale/service/Cashback';

describe('Calculating cashback', () => {
  test('Percent value must be 10 if value is less or equal than 1000', () => {
    expect.assertions(4);

    expect(CashbackService.calculate(1000).cashback_percent).toStrictEqual(10);
    expect(CashbackService.calculate(1000).cashback_value).toStrictEqual(100);

    expect(CashbackService.calculate(1000).cashback_percent).toStrictEqual(10);
    expect(CashbackService.calculate(250).cashback_value).toStrictEqual(25);
  });

  test('Percent value must be 15 if value is greater than 1000 and less or equal than 1500', () => {
    expect.assertions(4);

    expect(CashbackService.calculate(1500).cashback_percent).toStrictEqual(15);
    expect(CashbackService.calculate(1500).cashback_value).toStrictEqual(225);

    expect(CashbackService.calculate(1001).cashback_percent).toStrictEqual(15);
    expect(CashbackService.calculate(1001).cashback_value).toStrictEqual(
      150.15
    );
  });

  test('Percent value must be 20 if value is greater than 1500', () => {
    expect.assertions(4);

    expect(CashbackService.calculate(1501).cashback_percent).toStrictEqual(20);
    expect(CashbackService.calculate(1501).cashback_value).toStrictEqual(300.2);

    expect(CashbackService.calculate(3000).cashback_percent).toStrictEqual(20);
    expect(CashbackService.calculate(3000).cashback_value).toStrictEqual(600);
  });
});
