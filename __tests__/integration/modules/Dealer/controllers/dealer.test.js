import request from 'supertest';
import app from '../../../../../src/app';
import truncate from '../../../../util/truncate';

describe('Dealers', () => {
  beforeEach(async () => {
    truncate();
  });

  it('Should be possible to create a dealer', async done => {
    expect.assertions(2);

    const response = await request(app).post('/dealer').send({
      name: 'Marta Edivania',
      email: 'marta.edii@oboti.com',
      document: '42683718005',
      password: 'Admin123',
    });
    done();

    expect(response.body).toHaveProperty('id');
    expect(response.status).toStrictEqual(201);
  });

  it('Should not be possible to create a duplicated dealer', async done => {
    expect.assertions(1);

    await request(app).post('/dealer').send({
      name: 'Marta Edivania',
      email: 'marta.edii@oboti.com',
      document: '42683718005',
      password: 'Admin123',
    });

    const response = await request(app).post('/dealer').send({
      name: 'Marta Edivania',
      email: 'marta.edii@oboti.com',
      document: '42683718005',
      password: 'Admin123',
    });
    done();

    expect(response.status).toStrictEqual(400);
  });
});
