module.exports = {
  // bail: 1,

  clearMocks: true,

  collectCoverage: true,

  collectCoverageFrom: ['src/app/modules/**/controllers/**/*.js'],

  coverageDirectory: '__tests__/coverage',

  coverageProvider: 'v8',

  coverageReporters: ['text', 'lcov'],

  testEnvironment: 'node',

  testMatch: ['**/__tests__/**/*.test.js'],

  transform: {
    '.(js|jsx|ts|tsx)': '@sucrase/jest-plugin',
  },
};
