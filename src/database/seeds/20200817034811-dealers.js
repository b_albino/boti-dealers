const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcryptjs');

module.exports = {
  up: queryInterface =>
    queryInterface.bulkInsert(
      'dealers',
      [
        {
          id: uuidv4(),
          name: 'Terezinha Albino',
          email: 'terezinha.albino@oboti.com',
          document: '06255512061',
          password: bcrypt.hashSync('Admin123', 8),
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuidv4(),
          name: 'Alisson Morais',
          email: 'alisson.morais@oboti.com',
          document: '96570723014',
          password: bcrypt.hashSync('!Admin123', 8),
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    ),

  down: queryInterface => queryInterface.bulkDelete('dealers', null, {}),
};
