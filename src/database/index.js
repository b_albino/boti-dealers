import Sequelize from 'sequelize';

import databaseConfig from '../app/config/database';

import Dealer from '../app/modules/Dealer/models/Dealer';
import Sale from '../app/modules/Sale/models/Sale';

const models = [Dealer, Sale];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
