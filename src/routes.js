import Dealer from './app/modules/Dealer/routes/dealer';
import Session from './app/modules/Dealer/routes/session';
import Sale from './app/modules/Sale/routes/sale';
import Cashback from './app/modules/Cashback/routes/cashback';

export default [].concat(Cashback, Dealer, Session, Sale);
