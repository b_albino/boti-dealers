import { parseISO, isAfter } from 'date-fns';

import Sale from '../models/Sale';
import Dealer from '../../Dealer/models/Dealer';
import ReturnSale from '../dto/ReturnSale';
import CreateSale from '../dto/CreateSale';

class SaleController {
  async create(req, res) {
    const { code, date } = req.body;
    const { dealer_id } = req;
    req.body.dealer_id = dealer_id;

    const saleExists = await Sale.findOne({
      where: { code },
    });

    if (saleExists)
      return res.status(400).json({ error: 'Sale already exists' });

    const { document } = await Dealer.findByPk(dealer_id);

    if (!document) return res.status(400).json({ error: 'Dealer not found' });

    const isoDate = parseISO(date);
    if (isAfter(isoDate, new Date()))
      return res.status(400).json({ error: 'Future date are not permitted' });

    req.body.document = document;

    const sale = CreateSale.mapper(req.body);

    const savedSale = await Sale.create(sale);

    return res.status(201).json(savedSale);
  }

  async listAll(req, res) {
    const sales = await Sale.findAll({
      include: [
        {
          model: Dealer,
          as: 'dealer',
        },
      ],
    });

    return res.json(ReturnSale.mapper(sales));
  }

  async listByDealer(req, res) {
    const { dealer_id } = req;
    const sales = await Sale.findAll({
      where: { dealer_id },
    });

    return res.json(ReturnSale.mapper(sales));
  }
}

export default new SaleController();
