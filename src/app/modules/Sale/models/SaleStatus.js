const SaleStatus = {
  APPROVED: 'approved',
  VALIDATING: 'validating',
};

export default SaleStatus;
