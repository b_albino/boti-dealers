import Sequelize, { Model } from 'sequelize';
import { isBefore, subHours } from 'date-fns';

class Sale extends Model {
  static init(sequelize) {
    super.init(
      {
        code: Sequelize.STRING,
        date: Sequelize.DATE,
        canceled_at: Sequelize.DATE,
        value: Sequelize.DOUBLE,
        status: Sequelize.STRING,
        cashback_percent: Sequelize.DOUBLE,
        cashback_value: Sequelize.DOUBLE,
        past: {
          type: Sequelize.VIRTUAL,
          get() {
            return isBefore(this.date, new Date());
          },
        },
        cancelable: {
          type: Sequelize.VIRTUAL,
          get() {
            return isBefore(new Date(), subHours(this.date, 2));
          },
        },
      },
      { sequelize }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.Dealer, { foreignKey: 'dealer_id', as: 'dealer' });
  }
}

export default Sale;
