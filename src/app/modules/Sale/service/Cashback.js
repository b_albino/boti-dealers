class Cashback {
  calculate(value) {
    let cashback_percent, cashback_value;

    if (value <= 1000) {
      cashback_percent = 10;
      cashback_value = value * (cashback_percent / 100);
    }

    if (value > 1000 && value <= 1500) {
      cashback_percent = 15;
      cashback_value = value * (cashback_percent / 100);
    }

    if (value > 1500) {
      cashback_percent = 20;
      cashback_value = value * (cashback_percent / 100);
    }

    cashback_value = parseFloat(cashback_value.toFixed(2));

    return { cashback_percent, cashback_value };
  }
}

export default new Cashback();
