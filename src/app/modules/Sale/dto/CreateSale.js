import { v4 as uuidv4 } from 'uuid';
import CashbackService from '../service/Cashback';
import SaleStatus from '../models/SaleStatus';

class CreateSale {
  mapper(sale) {
    const { cashback_percent, cashback_value } = CashbackService.calculate(
      sale.value
    );

    return {
      id: uuidv4(),
      code: sale.code,
      dealer_id: sale.dealer_id,
      date: sale.date,
      value: sale.value,
      status:
        sale.document === process.env.APPROVED_DOCUMENT
          ? SaleStatus.APPROVED
          : SaleStatus.VALIDATING,
      cashback_percent,
      cashback_value,
    };
  }
}

export default new CreateSale();
