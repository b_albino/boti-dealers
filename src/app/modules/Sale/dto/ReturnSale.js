class ReturnSale {
  mapper(sales) {
    return sales.map(sale => {
      let dealer;

      const {
        id,
        code,
        date,
        canceled_at,
        value,
        status,
        cashback_percent,
        cashback_value,
      } = sale;

      if (sale.dealer) {
        const { id: delaer_id, name, email, document } = sale.dealer;
        dealer = {
          id: delaer_id,
          name,
          email,
          document,
        };
      }

      return {
        id,
        code,
        date,
        canceled_at,
        value,
        status,
        cashback_percent,
        cashback_value,
        dealer,
      };
    });
  }
}

export default new ReturnSale();
