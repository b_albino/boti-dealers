import { Router } from 'express';
import auth from '../../middleware/auth';
import CreateSale from '../../middleware/validations/CreateSale';

import Sale from '../controllers/Sale';

const routes = new Router();

routes.get('/sale', Sale.listAll);

routes.use(auth);

routes.post('/sale', CreateSale, Sale.create);
routes.get('/dealer/sale', Sale.listByDealer);

export default routes;
