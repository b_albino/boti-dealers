import api from '../services/api';
import ReturnCashbackMappter from '../dto/ReturnCashback';

class CashbackContoller {
  async getTotal(req, res) {
    const { cpf } = req.query;

    try {
      const { data } = await api.get(`v1/cashback?cpf=${cpf}`);

      if (data.statusCode === 400) {
        return res.status(400).json({ error: data.body.message });
      }
      return res.json(ReturnCashbackMappter.mapper(data, cpf));
    } catch (error) {
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  }
}

export default new CashbackContoller();
