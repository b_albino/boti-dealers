import { Router } from 'express';

import Sale from '../controllers/Cashback';

const routes = new Router();

routes.get('/cashback', Sale.getTotal);

export default routes;
