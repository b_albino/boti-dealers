import axios from 'axios';

const api = axios.create({
  baseURL: process.env.CASHBACK_URL,
});

api.interceptors.request.use(async config => {
  const configParam = config;
  configParam.headers.token = process.env.CASHBACK_TOKEN;
  return configParam;
});

export default api;
