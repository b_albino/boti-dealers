class ReturnCashback {
  mapper(data, cpf) {
    return {
      dealer: cpf,
      credit: data.body.credit,
    };
  }
}

export default new ReturnCashback();
