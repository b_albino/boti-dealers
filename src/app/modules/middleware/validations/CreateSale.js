import * as Yup from 'yup';

export default async (req, res, next) => {
  const schema = Yup.object().shape({
    code: Yup.string().required(),
    date: Yup.date().required(),
    value: Yup.number().required(),
  });

  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Validation error' });
  }
  return next();
};
