import Dealer from '../models/Dealer';
import CreateDealerMapper from '../dto/CreateDealer';

class DealerController {
  async create(req, res) {
    const userExists = await Dealer.findOne({
      where: { email: req.body.email },
    });

    if (userExists)
      return res.status(400).json({ error: 'Dealer already exists' });

    const dealer = CreateDealerMapper.mapper(req.body);

    const { id, name, email, document } = await Dealer.create(dealer);
    return res.status(201).json({ id, name, email, document });
  }
}

export default new DealerController();
