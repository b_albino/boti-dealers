import { Router } from 'express';

import Session from '../controllers/Session';

const routes = new Router();

routes.post('/login', Session.create);

export default routes;
