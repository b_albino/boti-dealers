import { Router } from 'express';
import DealerValidation from '../../middleware/validations/CreateDealer';

import Dealer from '../controllers/Dealer';

const routes = new Router();

routes.post('/dealer', DealerValidation, Dealer.create);

export default routes;
