const { v4: uuidv4 } = require('uuid');

class CreateDealer {
  mapper(dealer) {
    return {
      id: uuidv4(),
      ...dealer,
    };
  }
}

export default new CreateDealer();
