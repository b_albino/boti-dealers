![logo](.github/logo-boticario-preto.png)


## Benefícios para revendedores


Sistema de Cashback, onde o valor será disponibilizado como crédito para a próxima compra da revendedora no Boticário;

---

**Executando o projeto**

Após clonar o projeto, executar os seguintes comandos:

```bash
$ yarn # instalar as dependências - Pode usar npm install também
$ yarn dev # rodar o projeto - npm run dev`
```

**Executando os testes**

Na raiz do projeto, executar o comnado:

```bash
$ yarn test
```

---

**Rotas**

Essas sãos as rotas disponíveis na aplicação.

| Recurso        | Método   | Utilização                                                                                                                         |
| -------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| `/login`       | [`POST`] | Logar na aplicação                                                                                                                 |
| `/cashback`    | [`GET`]  | Exibir o acumulado de cashback a partir de um cpf enviado como query parameter                                                     |
| `/sale`        | [`POST`] | Cadastrar uma venda. Somente uma revendedora logada pode cadastrar uma venda. Deve ser enviado o Bearer Token para essa requisição |
| `/sale`        | [`GET`]  | Listar todas as vendas. Não precisa estar logado na aplicação                                                                      |
| `/dealer/sale` | [`GET`]  | Listar todas as vendas por revendedora. A revendedora deve estar logada e enviar o Bearer Token para essa requisição               |
| `/dealer`      | [`POST`] | Cadastrar uma revendedora. Não precisa estar logado na aplicação                                                                   |

**Tecnologias utilizadas**

- [Node](https://github.com/albino29/rocketseat-bootcamp/tree/6107a3f0dbd36b410658cdb2946725a0285d481c/node.org) \(12.18.3\)
- [Npm](https://github.com/albino29/rocketseat-bootcamp/tree/6107a3f0dbd36b410658cdb2946725a0285d481c/node.org) \(6.14.6\)
- [Yarn](https://github.com/albino29/rocketseat-bootcamp/tree/6107a3f0dbd36b410658cdb2946725a0285d481c/node.org) \(1.22.4\)
